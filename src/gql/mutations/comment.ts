import { gql } from "urql";

export const CreateCommentMutation = gql`
  mutation CreateComment($text: String!, $postId: ID!, $parentComment: ID) {
    createComment(text: $text, postId: $postId, parentComment: $parentComment) {
      success
      message
    }
  }
`;

export const LikeCommentMutation = gql`
  mutation LikeComment($commentId: ID!) {
    likeComment(commentId: $commentId) {
      success
      message
    }
  }
`;
