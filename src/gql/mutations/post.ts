import { gql } from "urql";

export const CreatePostMutation = gql`
  mutation CreatePost($title: String, $text: String!, $tags: [String]) {
    createPost(title: $title, text: $text, tags: $tags) {
      success
      message
    }
  }
`;

export const LikePostMutation = gql`
  mutation LikePost($postId: ID!) {
    likePost(postId: $postId) {
      success
      message
    }
  }
`;
