// src/urqlClient.js or similar file
import { cacheExchange } from "@urql/exchange-graphcache";
import { createClient, fetchExchange } from "urql";

const cache = cacheExchange({});

const client = createClient({
  url: "http://localhost:5000/graphql",
  exchanges: [cache, fetchExchange],
  fetchOptions: {
    credentials: "include",
  },
});

export default client;
