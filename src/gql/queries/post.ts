import { gql } from "urql";

export const PostQuery = gql`
  query GetPost($id: ID!) {
    getPost(id: $id) {
      id
      createdAt
      title
      text
      tags
      likes
      comments {
        id
        createdAt
        text
        likes
        author {
          id
          username
        }
      }
      author {
        id
        username
      }
    }
  }
`;
