import { gql } from "urql";

export const TimelineQuery = gql`
  query GetTimeline($offset: Int, $limit: Int) {
    getTimeline(offset: $offset, limit: $limit) {
      id
      createdAt
      title
      text
      tags
      likes
      comments {
        id
      }
      author {
        id
        username
      }
    }
  }
`;
