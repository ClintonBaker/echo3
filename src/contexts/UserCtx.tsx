import React, { createContext, useReducer, useState } from "react";

type UserT = {
  id: string;
  username: string;
  email: string;
};

type NewUserT = {
  username: string;
  email: string;
  password: string;
};

type ActionT = {
  type: "LOGIN" | "REGISTER" | "LOGOUT" | "SET_LOADING";
  payload?: any;
};

type UserStateT = {
  user: UserT | null;
  isAuthenticated: boolean;
  loading: boolean;
};

type UserCtxT = {
  user: UserT | null;
  userId: string;
  username: string;
  isAuthenticated: boolean;
  loading: boolean;
  logIn: (username: string, password: string) => Promise<void>;
  register: (newUser: NewUserT) => Promise<void>;
  logOut: () => Promise<void>;
  fetchUserData: () => Promise<void>;
};

const defaultContextValue: UserCtxT = {
  user: null,
  userId: "",
  username: "",
  isAuthenticated: false,
  loading: true,
  logIn: async () => {},
  register: async () => {},
  logOut: async () => {},
  fetchUserData: async () => {},
};

export const UserCtx = createContext<UserCtxT>(defaultContextValue);

const userReducer = (state: UserStateT, action: ActionT): UserStateT => {
  switch (action.type) {
    case "LOGIN":
      return {
        ...state,
        user: action.payload,
        isAuthenticated: true,
        loading: false,
      };
    case "REGISTER":
      return {
        ...state,
        user: action.payload,
        isAuthenticated: true,
        loading: false,
      };
    case "LOGOUT":
      return {
        ...state,
        user: action.payload,
        isAuthenticated: false,
        loading: false,
      };
    case "SET_LOADING":
      return { ...state, loading: action.payload };
    default:
      throw new Error(`Unknown action: ${action.type}`);
  }
};

export const UserProvider = ({ children }: { children: React.ReactNode }) => {
  const initialState: UserStateT = {
    user: null,
    isAuthenticated: false,
    loading: true,
  };
  const [state, dispatch] = useReducer(userReducer, initialState);

  const logIn = async (username: string, password: string) => {
    dispatch({ type: "SET_LOADING", payload: true });
    const res = await fetch("http://localhost:5000/login", {
      method: "POST",
      body: JSON.stringify({ username, password }),
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });

    const { user } = await res.json();

    if (user) {
      dispatch({ type: "LOGIN", payload: user });
    }
  };

  const register = async (newUser: NewUserT) => {
    dispatch({ type: "SET_LOADING", payload: true });
    const res = await fetch("http://localhost:5000/register", {
      method: "POST",
      body: JSON.stringify(newUser),
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });

    const { data } = await res.json();

    if (data) {
      dispatch({ type: "LOGIN", payload: data });
    }
  };

  const logOut = async () => {
    dispatch({ type: "SET_LOADING", payload: true });
    const res = await fetch("http://localhost:5000/logout", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });
    dispatch({ type: "LOGOUT" });
  };

  const fetchUserData = async () => {
    dispatch({ type: "SET_LOADING", payload: true });
    try {
      const res = await fetch("http://localhost:5000/verify-token", {
        method: "GET",
        credentials: "include",
      });
      const { user } = await res.json();

      if (user) {
        dispatch({ type: "LOGIN", payload: user });
      } else {
        dispatch({ type: "LOGOUT" });
      }
    } catch (err) {
      dispatch({ type: "LOGOUT" });
    }
  };

  return (
    <UserCtx.Provider
      value={{
        user: state.user,
        userId: state.user?.id || "",
        username: state.user?.username || "",
        isAuthenticated: state.isAuthenticated,
        loading: state.loading,
        logIn,
        register,
        logOut,
        fetchUserData,
      }}
    >
      {children}
    </UserCtx.Provider>
  );
};
