import { createContext, useContext, useState } from "react";
import { useQuery } from "urql";
import { TimelineQuery } from "@queries/timeline";
import { UserCtx } from "./UserCtx";

interface TimelineContextI {
  data: any;
  fetching: boolean;
  error: any;
  loadMore: () => void;
  refreshTL: () => void;
}

const defaultCtxValue: TimelineContextI = {
  data: null,
  fetching: false,
  error: null,
  loadMore: () => {},
  refreshTL: () => {},
};

export const TimelineCtx = createContext<TimelineContextI>(defaultCtxValue);

export const TimelineProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const { isAuthenticated } = useContext(UserCtx);
  const [pagination, setPagination] = useState({ page: 1, pageSize: 10 });
  const [result, reexecuteQuery] = useQuery({
    query: TimelineQuery,
    pause: !isAuthenticated,
    variables: { ...pagination },
  });
  const { data, fetching, error } = result;

  const loadMore = () => {
    setPagination((prev) => ({ ...prev, page: prev.page + 1 }));
    reexecuteQuery({ requestPolicy: "network-only" });
  };

  const refreshTL = () => {
    reexecuteQuery({ requestPolicy: "network-only" });
  };

  return (
    <TimelineCtx.Provider
      value={{ data, fetching, error, loadMore, refreshTL }}
    >
      {children}
    </TimelineCtx.Provider>
  );
};
