import { useContext, useEffect } from "react";
import { UserCtx } from "@contexts/UserCtx";
import { Outlet, useNavigate } from "react-router-dom";
import styles from "./Root.module.css";
import Nav from "../Nav/Nav";
import Spinner from "../Spinner/Spinner";

const Root = () => {
  const { isAuthenticated, loading, fetchUserData } = useContext(UserCtx);
  const navigate = useNavigate();

  useEffect(() => {
    fetchUserData();
  }, []);

  useEffect(() => {
    if (!loading && !isAuthenticated) {
      navigate("/login");
    }
  }, [loading, isAuthenticated, navigate]);

  if (loading) {
    return <Spinner />;
  }

  return (
    <div className={styles.AppLayout}>
      <Nav />
      <Outlet />
    </div>
  );
};

export default Root;
