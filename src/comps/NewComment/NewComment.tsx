import SendIcon from "@/assets/icons/send";
import styles from "./NewComment.module.css";
import ImageIcon from "@/assets/icons/image";
import GifIcon from "@/assets/icons/gif";
import { useCallback, useEffect, useRef, useState } from "react";
import { throttle } from "@/utils/lib";
import { CreateCommentMutation } from "@/gql/mutations/comment";
import { useMutation } from "urql";

type NewCommentT = {
  className?: string;
  postId: string;
  parentId?: string;
  focusInput?: boolean;
  onSuccess?: () => void;
};

const NewComment = ({
  className,
  postId,
  parentId,
  focusInput = false,
  onSuccess,
}: NewCommentT) => {
  const [commentData, setCommentData] = useState({ parentId, text: "" });
  const [result, createComment] = useMutation(CreateCommentMutation);
  const commentTextRef = useRef<HTMLTextAreaElement>(null);

  const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const { name, value } = e.target;
    setCommentData({
      ...commentData,
      [name]: value,
    });
  };

  const handleSubmit = useCallback(
    throttle(
      async (e: React.FormEvent<HTMLFormElement> | React.KeyboardEvent) => {
        e.preventDefault();
        const { text } = commentData;
        if (text.length < 1) return;
        const { data, error } = await createComment({
          text,
          postId,
          parentComment: null,
        });

        if (data.createComment.success) {
          setCommentData({ parentId, text: "" });
          onSuccess && onSuccess();
        }
      },
      300
    ),
    [commentData, createComment, postId, onSuccess]
  );

  useEffect(() => {
    focusInput && commentTextRef.current?.focus();
  }, [focusInput]);

  useEffect(() => {
    if (commentTextRef.current) {
      commentTextRef.current.style.height = "auto";
      commentTextRef.current.style.height = `${commentTextRef.current.scrollHeight}px`;
    }
  }, [commentData.text]);

  return (
    <div className={`${styles.commentInputContainer} ${className || ""}`}>
      <textarea
        className={styles.commentInput}
        name="text"
        ref={commentTextRef}
        placeholder="Write a comment..."
        value={commentData.text}
        onChange={handleChange}
        onKeyDown={(e) => {
          if (e.key === "Enter" && !e.shiftKey) {
            e.preventDefault();
            handleSubmit(e);
          }
        }}
        rows={1}
      />
      <div className={styles.commentControls}>
        <div className={styles.commentAddons}>
          <ImageIcon fontSize=".61rem" />
          <GifIcon fontSize=".6rem" />
        </div>
        <button className={styles.submitButton} onClick={handleSubmit}>
          <SendIcon fontSize=".9em" />
        </button>
      </div>
    </div>
  );
};

export default NewComment;
