import styles from "./Logo.module.css";

type LogoPropsT = {
  fontSize?: string;
};

const Logo = ({ fontSize }: LogoPropsT) => {
  return (
    <div
      className={styles.title}
      style={{ fontSize: fontSize ? fontSize : "4em" }}
    >
      Echo
    </div>
  );
};

export default Logo;
