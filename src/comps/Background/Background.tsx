import styles from "./Background.module.css";

type BGProps = {
  imgSrc: string;
  alt: string;
  opacity?: string;
};

export const Background = ({ imgSrc, alt, opacity }: BGProps) => {
  return (
    <>
      <div
        className={styles.overlay}
        style={{
          background: `rgba(0, 0, 0, ${opacity ? opacity : "0.8"})`,
        }}
      ></div>
      <img className={styles.bgImg} src={imgSrc} alt={alt} />
    </>
  );
};
