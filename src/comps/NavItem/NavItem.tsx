import { useNavigate } from "react-router-dom";
import styles from "./NavItem.module.css";

type NavItemT = {
  item: { label: string; Icon: React.ElementType; url: string };
};

const NavItem = ({ item }: NavItemT) => {
  const { label, Icon, url } = item;
  const navigate = useNavigate();
  const handleClick = () => {
    navigate(url);
  };
  return (
    <div className={styles.iconWrapper} onClick={handleClick}>
      <Icon fontSize=".625rem" />
      <span className={styles.iconLabel}>{label}</span>
    </div>
  );
};

export default NavItem;
