import { useContext, useEffect } from "react";
import styles from "./Timeline.module.css";
import { TimelineCtx } from "@/contexts/TimeLineCtx";
import TLPost from "../TLPost/TLPost";

const Timeline = () => {
  const { data, fetching, error, refreshTL } = useContext(TimelineCtx);

  useEffect(() => {
    refreshTL();
  }, []);

  if (fetching) return <p>Loading...</p>;
  if (error) return <p>Error loading posts!</p>;

  return (
    <div className={styles.main}>
      {data?.getTimeline.map((post: any) => (
        <TLPost key={post.id} post={post} />
      ))}
    </div>
  );
};

export default Timeline;
