import NewComment from "../NewComment/NewComment";
import styles from "./CommentBox.module.css";

type CommentBoxPropsT = {
  id: string;
  isVisible: boolean;
  isAnimating: boolean;
  toggleComment: () => void;
  handleCommentAnimationEnd: () => void;
};

const CommentBox = ({
  id,
  isVisible,
  isAnimating,
  toggleComment,
  handleCommentAnimationEnd,
}: CommentBoxPropsT) => {
  return (
    <div
      className={
        isVisible || isAnimating
          ? styles.showCommentSection
          : styles.hideCommentSection
      }
    >
      <div
        className={isVisible ? styles.showCommentBox : styles.hideCommentBox}
        onAnimationEnd={handleCommentAnimationEnd}
      >
        <NewComment
          postId={id}
          onSuccess={toggleComment}
          focusInput={isVisible}
        />
      </div>
    </div>
  );
};

export default CommentBox;
