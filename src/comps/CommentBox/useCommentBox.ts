import { useReducer } from "react";

type ReducerActions = { type: "toggle" | "animationEnd" };

const initialState = {
  isVisible: false,
  isAnimating: false,
};

function reducer(state: typeof initialState, action: ReducerActions) {
  switch (action.type) {
    case "toggle":
      return { ...state, isVisible: !state.isVisible, isAnimating: true };
    case "animationEnd":
      return { ...state, isAnimating: false };
    default:
      throw new Error();
  }
}

const useCommentBox = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const toggleComment = () => {
    dispatch({ type: "toggle" });
  };

  const handleAnimationEnd = () => {
    if (!state.isVisible) {
      dispatch({ type: "animationEnd" });
    }
  };

  return { state, toggleComment, handleAnimationEnd };
};

export default useCommentBox;
