import React from "react";
import styles from "./Timestamp.module.css";

type TimestampPropsT = {
  timestamp: string;
};

const formatPostTime = (timestamp: string) => {
  const postDate = new Date(parseInt(timestamp));
  const now = new Date();
  const secondsAgo = Math.round((now.getTime() - postDate.getTime()) / 1000);

  if (secondsAgo < 60) {
    return `${secondsAgo}s ago`;
  }

  const minutesAgo = Math.round(secondsAgo / 60);
  if (minutesAgo < 60) {
    return `${minutesAgo}m ago`;
  }

  const hoursAgo = Math.round(minutesAgo / 60);
  if (hoursAgo < 24) {
    return `${hoursAgo}h ago`;
  }

  const daysAgo = Math.round(hoursAgo / 24);
  if (daysAgo < 2) {
    return `${daysAgo}d ago`;
  }

  return postDate.toLocaleDateString("en-US", {
    month: "short",
    day: "numeric",
    year: "numeric",
  });
};

const Timestamp = React.memo(({ timestamp }: TimestampPropsT) => {
  return <span className={styles.timestamp}>{formatPostTime(timestamp)}</span>;
});

export default Timestamp;
