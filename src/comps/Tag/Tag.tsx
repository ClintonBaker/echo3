import { useEffect, useRef, useState } from "react";
import styles from "./Tag.module.css";
import CloseIcon from "@/assets/icons/close";

type TagT = {
  tagText: string;
  displayText?: string;
  maxWidth?: number;
  isNewTag?: boolean;
  saveTag: (tag: string) => void;
  removeTag?: () => void;
};

const Tag = ({
  tagText,
  displayText,
  maxWidth,
  isNewTag = false,
  saveTag,
  removeTag,
}: TagT) => {
  const [edit, setEdit] = useState(false);
  const [text, setText] = useState(tagText);
  const tagRef = useRef<HTMLTextAreaElement>(null);
  const textMeasureRef = useRef<HTMLSpanElement>(null);

  const adjustWidth = () => {
    const textSpan = textMeasureRef.current;
    const textarea = tagRef.current;
    if (textSpan && textarea) {
      textSpan.style.display = "inline";
      textSpan.textContent = text || "";
      textarea.style.width = `${textSpan.offsetWidth + 8}px`;
      textSpan.style.display = "none";
    }
  };

  const adjustHeight = () => {
    const textarea = tagRef.current;
    if (textarea) {
      textarea.style.height = "0px";
      textarea.style.padding = "0";
      textarea.style.width = `${maxWidth}px`;
      textarea.style.height = textarea.scrollHeight + "px";
    }
  };

  const adjustBox = () => {
    adjustHeight();
    adjustWidth();
  };

  const save = () => {
    saveTag(text);
    !isNewTag && setEdit(false);
    displayText && setText(tagText);
  };

  const handleBlur = () => {
    save();
    isNewTag && setEdit(false);
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.key === "Enter") {
      e.preventDefault();
      save();
    }
  };

  useEffect(() => {
    adjustBox();
  }, [text, edit]);

  useEffect(() => {
    if (tagRef.current) {
      adjustWidth();
      tagRef.current.focus();
    }
  }, [edit]);

  const handleChange = (e: any) => {
    setText(e.target.value);
  };

  return (
    <div
      className={styles.tag}
      style={{ maxWidth: maxWidth ? maxWidth - 5 : 500 }}
      onClick={() => {
        setEdit(true);
      }}
    >
      {edit ? (
        <div>
          <textarea
            className={styles.tagInput}
            ref={tagRef}
            value={text}
            rows={1}
            onChange={handleChange}
            onBlur={handleBlur}
            onKeyDown={handleKeyDown}
            style={{ maxWidth: maxWidth ? maxWidth - 26 : 500 }}
          />
          <span
            ref={textMeasureRef}
            style={{
              visibility: "hidden",
              position: "absolute",
              whiteSpace: "pre",
              fontSize: "0.8rem",
              fontFamily: "monospace",
            }}
          />
        </div>
      ) : (
        <div
          className={styles.displayContent}
          style={{ maxWidth: maxWidth ? maxWidth - 26 : 500 }}
        >
          <div className={styles.displayText}>{`${removeTag ? "#" : ""} ${
            displayText ? displayText : text
          }`}</div>
          {removeTag && (
            <CloseIcon
              className={styles.closeIcon}
              fontSize=".35rem"
              strokeWidth="3"
              handleClick={removeTag}
            />
          )}
        </div>
      )}
    </div>
  );
};

export default Tag;
