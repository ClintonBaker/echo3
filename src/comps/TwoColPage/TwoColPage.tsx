import styles from "./TwoColPage.module.css";

type TwoColPageT = {
  children: React.ReactNode;
};

const TwoColPage = ({ children }: TwoColPageT) => {
  return <div className={styles.TwoColSubGrid}>{children}</div>;
};

export default TwoColPage;
