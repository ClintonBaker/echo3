import ChatIcon from "@/assets/icons/chat";
import Logo from "../Logo/Logo";
import NavItem from "../NavItem/NavItem";
import styles from "./Nav.module.css";
import HomeIcon from "@assets/icons/home";
import NotificationIcon from "@/assets/icons/notifications";
import ProfileIcon from "@/assets/icons/profile";
import SearchIcon from "@/assets/icons/search";

const NavItems = [
  {
    label: "Home",
    Icon: HomeIcon,
    url: "/",
  },
  {
    label: "Explore",
    Icon: SearchIcon,
    url: "/explore",
  },
  {
    label: "Notifications",
    Icon: NotificationIcon,
    url: "/notifications",
  },
  {
    label: "Messages",
    Icon: ChatIcon,
    url: "/messages",
  },
  {
    label: "Profile",
    Icon: ProfileIcon,
    url: "/my-profile",
  },
];

const Nav = () => {
  return (
    <div className={styles.NavWrapper}>
      <div className={styles.logoContainer}>
        <Logo fontSize="3em" />
      </div>
      {NavItems.map((item, index) => (
        <NavItem key={index} item={item} />
      ))}
    </div>
  );
};

export default Nav;
