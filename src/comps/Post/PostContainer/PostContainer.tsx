import styles from "./PostContainer.module.css";

type PostContainerT = {
  children: React.ReactNode;
};

const PostContainer = ({ children }: PostContainerT) => {
  return (
    <div className={styles.postContainer}>
      <div className={styles.postWrapper}>{children}</div>
    </div>
  );
};

export default PostContainer;
