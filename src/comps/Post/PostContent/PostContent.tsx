import styles from "./PostContent.module.css";

type PostContentT = {
  title?: string;
  text: string;
  tags: [string];
};

const preventNav = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
  e.stopPropagation();
};

const PostContent = ({ title, text, tags }: PostContentT) => {
  return (
    <div className={styles.postContent}>
      {title && <h2 className={styles.postTitle}>{title}</h2>}
      {text && (
        <div className={styles.postText} onClick={preventNav}>
          {text}
        </div>
      )}
      {tags && (
        <div className={styles.postTags}>
          {tags.map((tag, index) => (
            <span key={index}>#{tag}</span>
          ))}
        </div>
      )}
    </div>
  );
};

export default PostContent;
