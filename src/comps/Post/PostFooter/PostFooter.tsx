import CommentIcon from "@/assets/icons/comment";
import styles from "./PostFooter.module.css";
import HeartIcon from "@/assets/icons/heart";
import EchoIcon from "@/assets/icons/echo";
import ShareIcon from "@/assets/icons/share";
import { useCallback, useEffect, useRef, useState } from "react";
import { LikePostMutation } from "@/gql/mutations/post";
import { useMutation } from "urql";
import { throttle } from "@/utils/lib";

type PostFooterT = {
  id: string;
  commentClass?: string;
  intLiked: boolean;
  numComments: number;
  numLikes: number;
  commentClick?: () => void;
};

const PostFooter = ({
  id,
  commentClass,
  intLiked,
  numComments,
  numLikes,
  commentClick,
}: PostFooterT) => {
  const initialRender = useRef(true);
  const [liked, setLiked] = useState(intLiked);
  const [_, likePost] = useMutation(LikePostMutation);
  const heartClass = `${
    liked ? (initialRender.current ? styles.liked : styles.likedAnimated) : ""
  }`;

  const handleLike = useCallback(
    throttle(() => {
      setLiked((prevState) => {
        likePost({ postId: id });
        return !prevState;
      });
    }, 300),
    [liked, id, likePost]
  );

  useEffect(() => {
    initialRender.current = false;
  }, []);

  return (
    <div className={styles.postFooter}>
      <div className={styles.iconSection}>
        <CommentIcon className={commentClass} handleClick={commentClick} />
        {!!numComments && (
          <div
            className={`${styles.sectionNum}${
              commentClass ? " " + styles.accent : ""
            }`}
          >
            {numComments}
          </div>
        )}
      </div>
      <div className={styles.iconSection}>
        <EchoIcon fontSize="1.3em" />
      </div>
      <div className={styles.iconSection}>
        <HeartIcon className={heartClass} handleClick={handleLike} />
        {!!numLikes && (
          <div
            className={`${styles.sectionNum}${
              heartClass ? " " + styles.accent : ""
            }`}
          >
            {numLikes}
          </div>
        )}
      </div>
      <div className={styles.iconSection}>
        <ShareIcon />
      </div>
    </div>
  );
};

export default PostFooter;
