import MiniAvatar from "@/comps/MiniAvatar/MiniAvatar";
import styles from "./PostHeader.module.css";
import Timestamp from "@/comps/Timestamp/Timestamp";

type PostHeaderT = {
  username: string;
  createdAt: string;
};

const PostHeader = ({ username, createdAt }: PostHeaderT) => {
  return (
    <div className={styles.contentHeader}>
      <div className={styles.userAvatar}>
        <MiniAvatar src={`http://localhost:5000/avatars/${username}`} />
      </div>
      <div>
        <div className={styles.postAuthor}>{username}</div>
        <Timestamp timestamp={createdAt} />
      </div>
    </div>
  );
};

export default PostHeader;
