import { useEffect, useRef, useState } from "react";
import styles from "./TagsSection.module.css";
import Tag from "../Tag/Tag";

type TagsSectionT = {
  tags: string[];
  updateTags: (tags: string[]) => void;
};

const TagsSection = ({ tags, updateTags }: TagsSectionT) => {
  const sectionRef = useRef<HTMLDivElement>(null);
  const [maxWidth, setMaxWidth] = useState<number | undefined>();

  const newTag = (tag: string) => {
    tag.length > 0 && tag !== "#" && updateTags([...tags, tag]);
  };

  const removeTag = (index: number) => {
    updateTags([...tags.slice(0, index), ...tags.slice(index + 1)]);
  };

  const saveTag = (index: number, tag: string) => {
    const newTags = tags;
    newTags[index] = tag;
    updateTags([...newTags]);
  };

  useEffect(() => {
    if (sectionRef.current) {
      setMaxWidth(sectionRef.current.offsetWidth);
    }
  }, []);

  return (
    <div className={styles.tagsSection} ref={sectionRef}>
      {tags.map((tag, index) => {
        return (
          <Tag
            key={`${tag}-${index}`}
            tagText={tag}
            maxWidth={maxWidth}
            saveTag={(text) => {
              saveTag(index, text);
            }}
            removeTag={() => {
              removeTag(index);
            }}
          />
        );
      })}
      <Tag
        displayText={
          !tags.length ? "# Add tags to help people find your post" : "+"
        }
        tagText=""
        isNewTag={true}
        maxWidth={maxWidth}
        saveTag={newTag}
      />
    </div>
  );
};

export default TagsSection;
