import { useContext, useEffect, useRef, useState } from "react";
import styles from "./NewPost.module.css";
import MiniAvatar from "../MiniAvatar/MiniAvatar";
import TagsSection from "../TagsSection/TagsSection";
import { useMutation } from "urql";
import { CreatePostMutation } from "@/gql/mutations/post";
import { throttle } from "@/utils/lib";
import { UserCtx } from "@/contexts/UserCtx";

type PostDataT = {
  title?: string;
  text: string;
  tags: string[];
};

const NewPost = () => {
  const [postData, setPostData] = useState<PostDataT>({
    title: "",
    text: "",
    tags: [],
  });
  const { username } = useContext(UserCtx);
  const [result, createPost] = useMutation(CreatePostMutation);
  const postTextRef = useRef<HTMLTextAreaElement>(null);

  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setPostData({
      ...postData,
      [name]: value,
    });
  };

  const handleSubmit = throttle(async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { title, text, tags } = postData;
    if (text.length < 1) return;
    const { data, error } = await createPost({
      title,
      text,
      tags,
    });
    if (data.createPost.success) {
      setPostData({ title: "", text: "", tags: [] });
    }
  }, 1000);

  useEffect(() => {
    if (postTextRef.current) {
      postTextRef.current.style.height = "auto";
      postTextRef.current.style.height = `${postTextRef.current.scrollHeight}px`;
    }
  }, [postTextRef.current, postData.text]);

  return (
    <div className={styles.newPostContainer}>
      <div className={styles.contentContainer}>
        <div>
          <MiniAvatar src={`http://localhost:5000/avatars/${username}`} />
        </div>
        <div className={styles.postWrapper}>
          <input
            className={styles.postTitle}
            value={postData.title}
            name="title"
            placeholder="Title"
            onChange={handleChange}
          />
          <textarea
            className={styles.postText}
            name="text"
            value={postData.text}
            ref={postTextRef}
            placeholder="What's up??"
            onChange={handleChange}
          />
          <TagsSection
            tags={postData.tags}
            updateTags={(newTags) => {
              setPostData({ ...postData, tags: [...newTags] });
            }}
          />
        </div>
      </div>
      <div className={styles.buttonContainer} onClick={handleSubmit}>
        <button className={styles.postButton}>Post</button>
      </div>
    </div>
  );
};

export default NewPost;
