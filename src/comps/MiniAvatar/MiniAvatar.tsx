import styles from "./MiniAvatar.module.css";

type MiniAvatarT = {
  src: string;
};

const MiniAvatar = ({ src }: MiniAvatarT) => {
  return (
    <div className={styles.miniAvatarWrapper}>
      <img className={styles.miniAvatar} src={src} />
    </div>
  );
};

export default MiniAvatar;
