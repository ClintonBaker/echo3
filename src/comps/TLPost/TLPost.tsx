import { useContext } from "react";
import { UserCtx } from "@/contexts/UserCtx";
import styles from "./TLPost.module.css";
import { useNavigate } from "react-router-dom";
import PostContainer from "../Post/PostContainer/PostContainer";
import PostHeader from "../Post/PostHeader/PostHeader";
import PostContent from "../Post/PostContent/PostContent";
import PostFooter from "../Post/PostFooter/PostFooter";
import useCommentBox from "../CommentBox/useCommentBox";
import CommentBox from "../CommentBox/CommentBox";

type TLPostPropsT = {
  post: {
    id: string;
    createdAt: string;
    author: { id: string; username: string };
    title?: string;
    text: string;
    tags: [string];
    likes: [string];
    comments: [string];
  };
};

const TLPost = ({ post }: TLPostPropsT) => {
  const {
    id,
    author: { username },
    createdAt,
    title,
    text,
    tags,
    likes,
    comments,
  } = post;
  const { userId } = useContext(UserCtx);
  const {
    state: { isVisible, isAnimating },
    toggleComment,
    handleAnimationEnd,
  } = useCommentBox();
  const navigate = useNavigate();

  const postClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const target = e.target as HTMLElement;
    if (target.nodeName === "DIV") {
      navigate(`/${username}/post/${id}`);
    }
  };

  return (
    <div className={styles.main} onClick={postClick}>
      <PostContainer>
        <PostHeader username={username} createdAt={createdAt} />
        <PostContent title={title} text={text} tags={tags} />
        <PostFooter
          id={id}
          commentClass={isVisible ? styles.showComment : ""}
          numComments={comments.length}
          numLikes={likes.length}
          intLiked={likes.indexOf(userId) >= 0}
          commentClick={toggleComment}
        />
        <CommentBox
          id={id}
          isVisible={isVisible}
          isAnimating={isAnimating}
          toggleComment={toggleComment}
          handleCommentAnimationEnd={handleAnimationEnd}
        />
      </PostContainer>
    </div>
  );
};

export default TLPost;
