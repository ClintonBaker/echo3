import { useState } from "react";
import styles from "./SearchPanel.module.css";
import SearchIcon from "@/assets/icons/search";

const SearchPanel = () => {
  const [search, setSearch] = useState("");
  const [searchFocused, setSearchFocused] = useState(false);

  const handleFocus = () => {
    setSearchFocused(true);
  };

  const handleBlur = () => {
    setSearchFocused(false);
  };

  const handleChange = (e) => {
    setSearch(e.target.value);
  };

  return (
    <div className={styles.searchPanel}>
      <div className={styles.searchContainer}>
        <SearchIcon
          className={styles.searchInputIcon}
          stroke={searchFocused ? "var(--accent)" : "#aaa"}
          strokeWidth=".6"
          fontSize=".7rem"
        />
        <input
          className={styles.searchBar}
          onFocus={handleFocus}
          onBlur={handleBlur}
          onChange={handleChange}
          value={search}
          placeholder="Search"
        />
      </div>
    </div>
  );
};

export default SearchPanel;
