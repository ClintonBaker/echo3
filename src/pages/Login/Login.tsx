import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import styles from "./Login.module.css";
import { UserCtx } from "@contexts/UserCtx";
import Logo from "@/comps/Logo/Logo";
import { Background } from "@/comps/Background/Background";

import loginBG from "@assets/imgs/LoginBG.png";

export const Login = () => {
  const { user, logIn, register } = useContext(UserCtx);
  const navigate = useNavigate();
  const [isLogin, setIsLogin] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  useEffect(() => {
    user && navigate("/");
  }, [user]);

  const onSubmit = async (e: any) => {
    e.preventDefault();
    if (errorMsg) setErrorMsg("");
    if (
      !isLogin &&
      e.currentTarget.password.value !== e.currentTarget.rpassword.value
    ) {
      setErrorMsg(`The passwords don't match`);
      return;
    }

    const body = {
      username: e.currentTarget.username.value,
      password: e.currentTarget.password.value,
      email: isLogin ? null : e.currentTarget.email.value,
    };

    isLogin ? logIn(body.username, body.password) : register(body);
  };

  return (
    <div className={styles.Container}>
      <Background
        imgSrc={loginBG}
        opacity={"0.8"}
        alt="Swirtly background for Login Page"
      />
      <div className={styles.LogoContainer}>
        <Logo fontSize={"20em"} />
      </div>
      <div className={styles.formWrapper}>
        <h1>Join the coversation</h1>
        <h2>Echo today.</h2>
        <form className={styles.Form} onSubmit={onSubmit}>
          {!isLogin && (
            <input type="text" name="email" placeholder="Email" required />
          )}

          <input type="text" name="username" placeholder="Username" required />

          <input
            type="password"
            name="password"
            placeholder="Password"
            required
          />

          {!isLogin && (
            <input
              type="password"
              name="rpassword"
              placeholder="Verify Password"
              required
            />
          )}

          {errorMsg && <p className={styles.Error}>*{errorMsg}</p>}

          <div className={styles.Submit}>
            <button type="submit">{isLogin ? "Login" : "Signup"}</button>
            <a
              className={styles.Link}
              onClick={(e) => {
                e.preventDefault();
                setIsLogin(!isLogin);
              }}
            >
              {isLogin
                ? "I don't have an account"
                : "I already have an account"}
            </a>
          </div>
        </form>
      </div>
    </div>
  );
};
