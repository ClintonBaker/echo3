import { UserCtx } from "@/contexts/UserCtx";
import { useContext } from "react";

const MyProfile = () => {
  const { logOut } = useContext(UserCtx);
  return (
    <div>
      <button onClick={logOut}>Logout</button>
    </div>
  );
};

export default MyProfile;
