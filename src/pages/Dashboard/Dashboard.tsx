import styles from "./Dashboard.module.css";
import TwoColPage from "@/comps/TwoColPage/TwoColPage";
import NewPost from "@/comps/NewPost/NewPost";
import SearchPanel from "@/comps/SearchPanel/SearchPanel";
import Timeline from "@/comps/Timeline/Timeline";

export const Dashboard = () => {
  return (
    <TwoColPage>
      <div className={styles.content}>
        <div className={styles.newPostContainer}>
          <NewPost />
        </div>
        <Timeline />
      </div>
      <div>
        <SearchPanel />
      </div>
    </TwoColPage>
  );
};
