import PostHeader from "@/comps/Post/PostHeader/PostHeader";
import styles from "./PostDetails.module.css";

import SearchPanel from "@/comps/SearchPanel/SearchPanel";
import Spinner from "@/comps/Spinner/Spinner";
import TwoColPage from "@/comps/TwoColPage/TwoColPage";
import { UserCtx } from "@/contexts/UserCtx";
import { PostQuery } from "@/gql/queries/post";
import { useContext } from "react";
import { useParams } from "react-router-dom";
import { useQuery } from "urql";
import PostContainer from "@/comps/Post/PostContainer/PostContainer";
import PostContent from "@/comps/Post/PostContent/PostContent";
import PostFooter from "@/comps/Post/PostFooter/PostFooter";
import NewComment from "@/comps/NewComment/NewComment";
import Timestamp from "@/comps/Timestamp/Timestamp";
import MiniAvatar from "@/comps/MiniAvatar/MiniAvatar";

export const PostDetails = () => {
  const { isAuthenticated, userId } = useContext(UserCtx);
  const { postId } = useParams<{ postId: string }>();
  const [post, getPost] = useQuery({
    query: PostQuery,
    pause: !isAuthenticated,
    variables: { id: postId },
  });
  const { data, fetching, error } = post;

  if (fetching) return <Spinner />;

  const {
    id,
    author: { username },
    createdAt,
    title,
    text,
    tags,
    likes,
    comments,
  } = data.getPost;

  return (
    <TwoColPage>
      <div className={styles.main}>
        <div className={styles.header}>
          <span className={styles.backArrow}>{"\u2190"}</span> Back
        </div>
        <div className={styles.postWrapper}>
          <PostContainer>
            <PostHeader username={username} createdAt={createdAt} />
            <PostContent title={title} text={text} tags={tags} />
            <PostFooter
              id={id}
              numComments={comments.length}
              numLikes={likes.length}
              intLiked={likes.indexOf(userId) >= 0}
            />
            <NewComment className={styles.newComment} postId={id} />
          </PostContainer>
        </div>
        <div className={styles.commentsSection}>
          {comments.map((comment: any) => (
            <div key={comment.id} className={styles.comment}>
              <div className={styles.commentHeader}>
                <div className={styles.userAvatar}>
                  <MiniAvatar
                    src={`http://localhost:5000/avatars/${comment.author.username}`}
                  />
                </div>
                <div>
                  <div className={styles.commentAuthor}>
                    {comment.author.username}
                  </div>
                  <Timestamp timestamp={comment.createdAt} />
                </div>
              </div>
              <div className={styles.commentText}>{comment.text}</div>
            </div>
          ))}
        </div>
      </div>
      <div>
        <SearchPanel />
      </div>
    </TwoColPage>
  );
};
