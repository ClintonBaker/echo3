export type IconT = {
  width?: string;
  height?: string;
  fill?: string;
  stroke?: string;
  strokeWidth?: string;
  fontSize?: string;
  className?: string;
  handleClick?: () => void;
};
