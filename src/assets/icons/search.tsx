import { IconT } from "@/types/Icon";

const SearchIcon = (props: IconT) => {
  const { className, width, height, strokeWidth, fontSize, handleClick } =
    props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "1em" }}
      onClick={handleClick}
    >
      <svg
        width={width || "1.75151em"}
        height={height || "1.76035em"}
        viewBox="0 0 7.4147157 7.4521275"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1" transform="translate(0.88742311,-0.84708568)">
          <g
            id="g2"
            transform="matrix(1.2494282,0,0,1.2491878,-0.06104468,-0.64877504)"
            style={{ strokeWidth: strokeWidth || "0.800443" }}
          >
            <circle
              style={{
                fill: "none",
                stroke: "var(--icon-color, white)",
                strokeWidth: strokeWidth || "0.800443",
                strokeLinecap: "square",
                strokeLinejoin: "round",
              }}
              id="path1"
              cx="1.8641193"
              cy="3.7229912"
              r="2.1165221"
            />
            <path
              style={{
                fill: "none",
                stroke: "var(--icon-color, white)",
                strokeWidth: strokeWidth || "0.800443",
                strokeLinecap: "round",
                strokeLinejoin: "round",
                strokeDasharray: "none",
              }}
              d="m 3.3420024,5.2319654 1.55241,1.55241"
              id="path2"
            />
          </g>
        </g>
      </svg>
    </div>
  );
};

export default SearchIcon;
