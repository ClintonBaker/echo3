import { IconT } from "@/types/Icon";

const SendIcon = (props: IconT) => {
  const { className, width, height, fontSize, handleClick } = props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "1em" }}
      onClick={handleClick}
    >
      <svg
        width={width || "1.935em"}
        height={height || "1.52041em"}
        viewBox="0 0 8.1915132 6.4364116"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1" transform="translate(-0.13757676,-1.0151274)">
          <path
            style={{
              fill: "var(--icon-color, white)",
              stroke: "var(--icon-color, white)",
              strokeWidth: "1.064",
              strokeLinecap: "square",
              strokeLinejoin: "round",
              strokeDasharray: "none",
              paintOrder: "stroke fill markers",
            }}
            d="M 0.66955207,6.9195729 7.7971875,4.2333338 0.66955207,1.5470944 1.1810984,3.4756308 c 4.9130757,0.7818682 4.2959786,0.773854 0.030066,1.5191728 z"
            id="path2"
          />
        </g>
      </svg>
    </div>
  );
};

export default SendIcon;
