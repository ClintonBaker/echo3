import { IconT } from "@/types/Icon";

const GifIcon = (props: IconT) => {
  const { className, fontSize, width, height, strokeWidth, handleClick } =
    props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "1rem" }}
      onClick={handleClick}
    >
      <svg
        width={width || "3.02475em"}
        height={height || "2.12em"}
        viewBox="0 0 12.804775 8.9746664"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1" transform="translate(-6.6369834,-12.548348)">
          <g id="g1">
            <path
              d="m 10.642198,16.936874 h 1.219906 v 1.728612 q -0.286456,0.09384 -0.57785,0.138289 -0.291395,0.04445 -0.661812,0.04445 -0.548217,0 -0.9235722,-0.217312 -0.3753556,-0.22225 -0.5729112,-0.627239 -0.1926167,-0.409927 -0.1926167,-0.968022 0,-0.553156 0.2173111,-0.958145 0.2173112,-0.404989 0.6223002,-0.627239 0.4099278,-0.227189 0.9877778,-0.227189 0.296334,0 0.558095,0.05433 0.2667,0.05433 0.493889,0.153106 l -0.167923,0.385233 q -0.187677,-0.08396 -0.424744,-0.143228 -0.232128,-0.05927 -0.484011,-0.05927 -0.632178,0 -0.9877783,0.380294 -0.3506612,0.380295 -0.3506612,1.042106 0,0.419806 0.1333501,0.745772 0.1382889,0.321028 0.4296834,0.503767 0.291395,0.1778 0.765528,0.1778 0.232128,0 0.395111,-0.02469 0.162984,-0.02469 0.296334,-0.05927 v -1.047045 h -0.775406 z m 3.454051,1.861962 h -1.274234 v -0.256823 l 0.414867,-0.09384 v -2.820107 l -0.414867,-0.09878 v -0.256823 h 1.274234 v 0.256823 l -0.414867,0.09878 v 2.820107 l 0.414867,0.09384 z m 1.522945,0 h -0.4445 v -3.526368 h 1.970617 v 0.390173 h -1.526117 v 1.249539 h 1.432278 v 0.390172 h -1.432278 z"
              id="text1"
              style={{
                fontSize: "4.93889px",
                letterSpacing: "0.396875px",
                fill: "var(--icon-color, white)",
                stroke: "var(--icon-color, white)",
                strokeWidth: "0.302154",
                strokeLinecap: "round",
                strokeLinejoin: "round",
                strokeDasharray: "none",
                paintOrder: "stroke fill markers",
              }}
              aria-label="GIF"
            />
            <path
              id="rect1"
              style={{
                fill: "none",
                stroke: "var(--icon-color, white)",
                strokeWidth: "0.566737",
                strokeLinecap: "round",
                strokeLinejoin: "round",
                paintOrder: "stoke fill markers",
              }}
              d="M 7.3949537,12.831717 H 18.683702 c 0.262928,0 0.4746,0.211671 0.4746,0.474599 v 7.458673 c 0,0.262928 -0.211672,0.4746 -0.4746,0.4746 H 7.3949537 c -0.2629288,0 -0.4746008,-0.211672 -0.4746008,-0.4746 v -7.458673 c 0,-0.262928 0.211672,-0.474599 0.4746008,-0.474599 z"
            />
          </g>
        </g>
      </svg>
    </div>
  );
};

export default GifIcon;
