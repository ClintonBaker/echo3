import { IconT } from "@/types/Icon";

const EditIcon = (props: IconT) => {
  const { className, width, height, strokeWidth, fontSize, handleClick } =
    props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "1em" }}
      onClick={handleClick}
    >
      <svg
        width={width || "1.887em"}
        height={height || "1.88678em"}
        viewBox="0 0 7.9882921 7.98735"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1" transform="translate(-0.23918779,-0.51900102)">
          <path
            id="rect6"
            style={{
              fill: "none",
              stroke: "var(--icon-color, white)",
              strokeWidth: strokeWidth || "0.264583",
              strokeLinecap: "square",
              strokeLinejoin: "round",
              strokeDasharray: "none",
              paintOrder: "stroke fill markers",
            }}
            d="M 1.4142891,6.1836199 0.88647504,7.2729189 0.3714651,8.3740737 1.4730931,7.8595375 2.5623914,7.331723 7.1102232,2.7838873 5.9621209,1.6357841 Z M 6.2561413,1.3427119 7.4037694,2.4903409 8.0951916,1.7989182 C 7.8012811,1.2825633 7.4181248,0.92603139 6.9475635,0.65128918 Z"
          />
        </g>
      </svg>
    </div>
  );
};

export default EditIcon;
