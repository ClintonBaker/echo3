import { IconT } from "@/types/Icon";

const HomeIcon = (props: IconT) => {
  const { className, width, height, strokeWidth, fontSize, handleClick } =
    props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "1em" }}
      onClick={handleClick}
    >
      <svg
        width={width || "1.97581em"}
        height={height || "1.95889em"}
        viewBox="0 0 31.612902 31.342196"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1" transform="translate(-0.19354886,-0.32890267)">
          <path
            style={{
              fill: "none",
              stroke: "var(--icon-color, white)",
              strokeWidth: strokeWidth || "2.643",
              strokeLinecap: "square",
              strokeLinejoin: "round",
              strokeDasharray: "none",
            }}
            d="M 1.5150488,11.5503 15.999999,1.6503747 30.484951,11.5503 V 30.349599 L 18.766836,30.203505 V 19.498816 H 13.233163 V 30.203505 L 1.5150488,30.349599 Z"
            id="path3"
          />
        </g>
      </svg>
    </div>
  );
};

export default HomeIcon;
