import { IconT } from "@/types/Icon";

const HeartIcon = (props: IconT) => {
  const { className, width, height, strokeWidth, fontSize, handleClick } =
    props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "1em" }}
      onClick={handleClick}
    >
      <svg
        width={width || "2.149em"}
        height={height || "1.97131em"}
        viewBox="0 0 34.383998 31.541"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1" transform="translate(-6.8080285,-9.0621852)">
          <path
            id="rect6"
            style={{
              fill: "var(--icon-fill, none)",
              stroke: "var(--icon-color, white)",
              strokeWidth: strokeWidth || "4",
              strokeLinecap: "square",
              strokeLinejoin: "round",
              strokeDasharray: "none",
              paintOrder: "stroke fill markers",
            }}
            d="m 11.943528,14.8746 a 9.0810341,8.1785694 45.591344 0 0 -1.703171,3.409283 9.0810341,8.1785694 45.591344 0 0 2.666692,8.461626 l 11.59298,11.61796 11.592977,-11.61796 a 8.1785694,9.0810341 44.408656 0 0 2.666693,-8.461626 8.1785694,9.0810341 44.408656 0 0 -5.776736,-5.789184 8.1785694,9.0810341 44.408656 0 0 -8.443428,2.67244 l -0.03951,-0.03959 -0.03951,0.03959 a 9.0810341,8.1785694 45.591344 0 0 -8.443423,-2.67244 9.0810341,8.1785694 45.591344 0 0 -4.073564,2.379901 z"
          />
        </g>
      </svg>
    </div>
  );
};

export default HeartIcon;
