import { IconT } from "@/types/Icon";

const ChatIcon = (props: IconT) => {
  const { className, width, height, strokeWidth, fontSize, handleClick } =
    props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "1em" }}
      onClick={handleClick}
    >
      <svg
        width={width || "1.89418em"}
        height={height || "1.79992em"}
        viewBox="0 0 30.306868 28.798702"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1" transform="translate(0,-0.95219331)">
          <path
            id="path1"
            style={{
              fill: "none",
              stroke: "var(--icon-color, white)",
              strokeWidth: strokeWidth || "2.5",
              strokeLinecap: "square",
              strokeLinejoin: "round",
              strokeDasharray: "none",
              paintOrder: "stroke fill markers",
            }}
            d="M 15.15532,1.6021933 C 7.1436502,1.6018815 0.64900876,7.6303431 0.65000009,15.066333 0.64900779,22.502324 7.1436502,28.530787 15.15532,28.530476 c 2.573244,-0.01025 4.848185,-0.493183 7.063985,-1.707574 v 0 l 3.132004,1.318459 2.875405,0.959566 -0.607534,-2.973143 -0.728285,-3.1507 v 0 c 1.938624,-2.33931 2.753812,-4.959513 2.765973,-7.910751 C 29.657812,7.6317099 23.165517,1.6038146 15.15532,1.6021933 Z"
          />
        </g>
      </svg>
    </div>
  );
};

export default ChatIcon;
