import { IconT } from "@/types/Icon";

const TrashIcon = (props: IconT) => {
  const { width, height, fontSize, handleClick } = props;
  return (
    <div
      style={{ display: "inline-block", fontSize: fontSize || "1em" }}
      onClick={handleClick}
    >
      <svg
        width={width || "1.41274em"}
        height={height || "1.94240em"}
        viewBox="0 0 5.9805951 8.2228386"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1" transform="translate(-0.87525706,-2.4443297e-4)">
          <g
            id="g4"
            transform="matrix(1.1280739,0,0,1.1253151,-0.90318641,-0.74735882)"
            style={{ strokeWidth: "0.887553" }}
          >
            <path
              style={{
                fill: "none",
                stroke: "var(--icon-color, white)",
                strokeWidth: "0.529167",
                strokeLinecap: "round",
                strokeLinejoin: "round",
                strokeDasharray: "none",
                paintOrder: "stroke fill markers",
              }}
              d="M 1.753714,1.3071209 2.0856333,7.7943105 H 6.3362799 L 6.6889441,1.3278138"
              id="path1"
            />
            <path
              style={{
                fill: "none",
                stroke: "var(--icon-color, white)",
                strokeWidth: "0.236243",
                strokeLinecap: "round",
                strokeLinejoin: "round",
                strokeDasharray: "none",
                paintOrder: "stroke fill markers",
              }}
              d="M 3.4607346,6.9504128 V 1.5162538"
              id="path2"
            />
            <path
              style={{
                fill: "none",
                stroke: "var(--icon-color, white)",
                strokeWidth: "0.236243",
                strokeLinecap: "round",
                strokeLinejoin: "round",
                strokeDasharray: "none",
                paintOrder: "stroke fill markers",
              }}
              d="M 5.0286164,6.9504128 V 1.5162538"
              id="path3"
            />
            <path
              style={{
                fill: "none",
                stroke: "var(--icon-color, white)",
                strokeWidth: "0.354366",
                strokeLinecap: "round",
                strokeLinejoin: "round",
                strokeDasharray: "none",
                paintOrder: "stroke fill markers",
              }}
              d="m 1.7657182,0.84153315 4.9352301,0.020693"
              id="path4"
            />
          </g>
        </g>
      </svg>
    </div>
  );
};

export default TrashIcon;
