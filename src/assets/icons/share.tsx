import { IconT } from "@/types/Icon";

const ShareIcon = (props: IconT) => {
  const { className, width, height, strokeWidth, fontSize, handleClick } =
    props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "rem" }}
      onClick={handleClick}
    >
      <svg
        width={width || "2.15em"}
        height={height || "1.55464em"}
        viewBox="0 0 9.1016255 6.5812941"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1" transform="translate(-1.2754746,-3.9226901)">
          <path
            id="rect1"
            style={{
              fill: "none",
              stroke: "var(--icon-color, white)",
              strokeWidth: strokeWidth || "1.16417",
              strokeLinecap: "round",
              strokeLinejoin: "round",
              strokeDasharray: "none",
              paintOrder: "stroke fill markers",
            }}
            d="M 6.035175,4.5047178 V 6.1654559 C 3.8420579,6.522136 2.354678,6.9918236 1.8575284,9.4360428 v 0 C 2.8294345,8.7023636 4.1134375,7.719525 6.035175,8.2612174 V 9.9219556 L 7.9146501,8.5671679 9.7950587,7.2133366 7.9146501,5.8595052 Z"
          />
        </g>
      </svg>
    </div>
  );
};

export default ShareIcon;
