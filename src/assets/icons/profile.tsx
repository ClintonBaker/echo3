import { IconT } from "@/types/Icon";

const ProfileIcon = (props: IconT) => {
  const { className, width, height, strokeWidth, fontSize, handleClick } =
    props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "rem" }}
      onClick={handleClick}
    >
      <svg
        width={width || "1.07897em"}
        height={height || "1.86306em"}
        viewBox="0 0 4.567654 7.886955"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1" transform="translate(-1.57997,-0.02527259)">
          <path
            id="path1"
            style={{
              fill: "none",
              stroke: "var(--icon-color, white)",
              strokeWidth: strokeWidth || "0.537104",
              strokeLinecap: "round",
              strokeLinejoin: "round",
              strokeDasharray: "none",
            }}
            d="M 3.8640638,0.29382468 A 1.4952488,1.4572433 0 0 0 2.3683091,1.751561 1.4952488,1.4572433 0 0 0 3.8640638,3.208477 1.4952488,1.4572433 0 0 0 5.3589767,1.751561 1.4952488,1.4572433 0 0 0 3.8640638,0.29382468 Z m 0,3.97534632 c -1.1160632,0 -2.0151024,0.5337633 -2.0151024,1.1960493 v 1.5356683 c 0,0.6220703 0.018239,0.6130119 2.000793,0.6324788 2.0385051,0.020016 2.0285699,0.029807 2.0285699,-0.6324788 V 5.4652203 c 0,-0.662286 -0.8981973,-1.1960493 -2.0142605,-1.1960493 z"
          />
        </g>
      </svg>
    </div>
  );
};

export default ProfileIcon;
