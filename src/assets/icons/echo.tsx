import { IconT } from "@/types/Icon";

const EchoIcon = (props: IconT) => {
  const { className, width, height, strokeWidth, fontSize, handleClick } =
    props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "1em" }}
      onClick={handleClick}
    >
      <svg
        width={width || "2em"}
        height={height || "1.14148em"}
        viewBox="0 0 8.4666661 5.0322561"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1" transform="translate(0.88742311,-2.6149993)">
          <path
            id="path22"
            style={{
              fill: "var(--icon-color, white)",
              stroke: "var(--icon-color, white)",
              strokeWidth: strokeWidth || ".1",
              strokeLinejoin: "round",
              strokeDasharray: "none",
              paintOrder: "stroke fill markers",
            }}
            d="m 0.48849291,2.6150019 -1.37591602,1.8686468 h 0.80798217 V 6.8811238 A 0.56683543,0.56683543 0 0 0 0.48909451,7.4472527 H 5.0199313 L 4.1860793,6.3149947 H 1.0552236 V 4.4836487 H 1.864409 Z m 1.18339609,0 0.8338521,1.132258 H 5.6365968 V 5.578606 H 4.8274114 L 6.2033275,7.4472527 7.5792435,5.578606 H 6.7712613 V 3.1811309 A 0.56683543,0.56683543 0 0 0 6.2027258,2.6150019 Z"
          />
        </g>
      </svg>
    </div>
  );
};

export default EchoIcon;
