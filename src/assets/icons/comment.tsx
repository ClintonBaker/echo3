import { IconT } from "@/types/Icon";

const CommentIcon = (props: IconT) => {
  const { className, width, height, strokeWidth, fontSize, handleClick } =
    props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "1em" }}
      onClick={handleClick}
    >
      <svg
        width={width || "1.9130em"}
        height={height || "1.71875em"}
        viewBox="0 0 8.2083668 7.2760414"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g
          id="layer1"
          transform="matrix(1,0,0,0.97585031,0.42459831,0.33176685)"
        >
          <path
            id="rect1"
            style={{
              fill: "var(--icon-fill, none)",
              stroke: "var(--icon-color, white)",
              strokeWidth: strokeWidth || "1.15348",
              strokeLinecap: "square",
              strokeLinejoin: "round",
              strokeDasharray: "none",
            }}
            d="m 0.75755087,0.28676279 c -0.30750412,0 -0.5554092,0.30136637 -0.5554092,0.67614232 V 4.7663029 c 0,0.3747763 0.24790508,0.6761425 0.5554092,0.6761425 l 0.66382783,0.013906 v 0 0 l 0.354422,0.4620977 0.353788,0.4628678 0.334133,-0.4713679 0.3315969,-0.4675041 h 3.6970209 c 0.3075038,0 0.554775,-0.3013662 0.554775,-0.6761425 V 0.96290511 c 0,-0.37477595 -0.2472712,-0.67614232 -0.554775,-0.67614232 z"
          />
        </g>
      </svg>
    </div>
  );
};

export default CommentIcon;
