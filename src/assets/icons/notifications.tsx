import { IconT } from "@/types/Icon";

const NotificationIcon = (props: IconT) => {
  const { className, width, height, strokeWidth, fontSize, handleClick } =
    props;
  return (
    <div
      className={className}
      style={{ display: "inline-block", fontSize: fontSize || "rem" }}
      onClick={handleClick}
    >
      <svg
        width={width || "1.79661em"}
        height={height || "1.90912em"}
        viewBox="0 0 28.745821 30.545969"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1">
          <g
            id="g3"
            transform="matrix(1.1683986,0,0,1.1675347,-4.2609959,-0.57822246)"
            style={{
              strokeWidth: "1.71238",
              strokeDasharray: "none",
            }}
          >
            <path
              id="rect1"
              style={{
                fill: "none",
                stroke: "var(--icon-color, white)",
                strokeWidth: strokeWidth || "1.71238",
                strokeLinecap: "square",
                strokeLinejoin: "round",
                strokeDasharray: "none",
                paintOrder: "stroke fill markers",
              }}
              d="m 15.965909,1.3514408 c 5.868427,0 8.937925,2.7672241 9.287549,6.1643052 0.490758,4.768404 -0.03577,9.771566 1.952088,14.194486 1.399967,3.114883 -5.37121,1.126008 -11.239637,1.126008 -5.868427,0 -12.9917833,1.831568 -11.1715518,-1.057923 C 7.1098894,18.102571 6.6836018,12.352721 7.5002024,7.4190586 7.971397,4.5722387 10.097482,1.3514408 15.965909,1.3514408 Z"
            />
            <path
              style={{
                fill: "none",
                stroke: "var(--icon-color, white)",
                strokeWidth: strokeWidth || "1.71238",
                strokeLinecap: "square",
                strokeLinejoin: "round",
                strokeDasharray: "none",
                paintOrder: "stroke fill markers",
              }}
              id="path2"
              d="m 18.718709,22.969653 a 2.7555897,2.9656136 0 0 1 -2.745296,2.83219 2.7555897,2.9656136 0 0 1 -2.759588,-2.816057 l 2.752084,-0.149545 z"
            />
          </g>
        </g>
      </svg>
    </div>
  );
};

export default NotificationIcon;
