import { IconT } from "@/types/Icon";

const CloseIcon = (props: IconT) => {
  const { className, fontSize, width, height, strokeWidth, handleClick } =
    props;

  return (
    <div
      className={className}
      style={{ display: "flex", fontSize: fontSize || "1em" }}
      onClick={handleClick}
    >
      <svg
        width={width ? width : "2em"}
        height={height ? height : "2em"}
        viewBox="0 0 32 32"
        version="1.1"
        id="svg1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs id="defs1" />
        <g id="layer1">
          <path
            id="path1"
            style={{
              stroke: "var(--icon-color, white)",
              fill: "var(--icon-color, white)",
              strokeWidth: strokeWidth || "0.999998",
              strokeLinecap: "round",
              strokeLinejoin: "round",
              paintOrder: "stroke fill markers",
            }}
            d="m 2.0474676,0.39948274 a 1.6481429,1.6481429 0 0 0 -1.16498209,0.4837059 1.6481429,1.6481429 0 0 0 0,2.32996396 L 13.670035,16.000703 0.88248551,28.788252 a 1.6481429,1.6481429 0 0 0 0,2.329966 1.6481429,1.6481429 0 0 0 2.32996399,0 L 16,18.330667 28.787549,31.118218 a 1.6481429,1.6481429 0 0 0 2.329965,0 1.6481429,1.6481429 0 0 0 0,-2.329966 L 18.329963,16.000703 31.117514,3.2131526 a 1.6481429,1.6481429 0 0 0 0,-2.32996396 1.6481429,1.6481429 0 0 0 -1.164982,-0.4837059 1.6481429,1.6481429 0 0 0 -1.164983,0.4837059 L 16,13.670739 3.2124495,0.88318864 A 1.6481429,1.6481429 0 0 0 2.0474676,0.39948274 Z"
          />
        </g>
      </svg>
    </div>
  );
};

export default CloseIcon;
