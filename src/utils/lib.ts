export function throttle(func: any, wait: number) {
  let timer = Date.now();
  return function executeFunction(...args: any) {
    if (Date.now() - timer > wait) {
      func(...args);
      timer = Date.now();
    }
  };
}

export function debounce(func: any, wait: number) {
  let timeout: any;

  return function executedFunction(...args: any) {
    const later = () => {
      clearTimeout(timeout);
      func(...args);
    };

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
}
