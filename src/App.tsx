import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { UserProvider } from "@contexts/UserCtx";
import Root from "@comps/Root/Root";
import { Login } from "@pages/Login/Login";
import { Dashboard } from "@pages/Dashboard/Dashboard";
import { TimelineProvider } from "./contexts/TimeLineCtx";
import { Provider } from "urql";
import client from "./gql/urqlClient";
import { PostDetails } from "./pages/PostDetails/PostDetails";
import MyProfile from "./pages/MyProfile/MyProfile";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    children: [
      {
        index: true,
        element: <Dashboard />,
      },
      {
        path: ":username",
        children: [{ path: "post/:postId", element: <PostDetails /> }],
      },
      {
        path: "/my-profile",
        element: <MyProfile />,
      },
    ],
  },
  {
    path: "/login",
    element: <Login />,
  },
]);

function App() {
  return (
    <Provider value={client}>
      <UserProvider>
        <TimelineProvider>
          <RouterProvider router={router} />
        </TimelineProvider>
      </UserProvider>
    </Provider>
  );
}

export default App;
