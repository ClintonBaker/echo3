import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    port: 3000,
  },
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
      "@assets": path.resolve(__dirname, "./src/assets"),
      "@comps": path.resolve(__dirname, "./src/comps"),
      "@contexts": path.resolve(__dirname, "./src/contexts"),
      "@queries": path.resolve(__dirname, "./src/gql/queries"),
      "@mutations": path.resolve(__dirname, "./src/gql/mutations"),
      "@pages": path.resolve(__dirname, "./src/pages"),
      "@styles": path.resolve(__dirname, "./src/styles"),
      "@utils": path.resolve(__dirname, "./src/utils"),
    },
  },
});
